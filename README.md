# TRS-80 Color Computer Program Pak Case Models
by Mark J. Blair <nf6x@nf6x.net>

These are my solid models of cases for TRS-80 Color Computer Program Paks (ROM cartridges and hardware interfaces). I created these models, and I hereby place them in the public domain. All models are provided as-is with no warranty.

## File Types

Extension | File Type
----------|-----------
.co       | Original source files for the Cobalt modeling tool by Ashlar.
.stl      | STL surface models, one per part.
.sat      | ACIS solid models.
.stp      | STEP solid models, inch units.

## ProgramPak-Original-v1

This is my first attempt at a model of an original Color Computer Program Pak. It's as close as I could get to the original design.

![](ProgramPak-Original-v1-rendered.png?raw=true)

* I could not figure out how to model the edge blends of the finger notches accurately in my CAD tool. The top and bottom leading and trailing edges are square instead of curved.

* Fabricate the door return spring (not shown in model) from 0.024" diameter spring steel music wire. I used McMaster-Carr part number 9666K22. Don't cut this hard steel wire with your good electronics wire cutters. Use cutters made for cutting spring steel wire, such as McMaster-Carr part number 59445A45.

* When I had a prototype SLS printed by ShapeWays, the spring tabs were not strong enough, and one was broken off and missing when I received the order. I suspect that the tab broke off during vibratory polishing. The notch in the door for the return spring also came out a little bit too narrow, and will need to be widened slightly to keep the spring from jumping out.

* For the case screw, use a #6-32 flat head thread-cutting screw, 3/8" long. I used McMaster-Carr part number 90086A146.

* For the two PCB mounting screws, use #4 pan head sheet metal screws, 3/8" long. I used McMaster-Carr part number 90190A108.

